Nightmare Creator
======
## What is this tool

## Command list

Comments can be made by using \\ before any text

_-loc <directory_path>_ or _-l <directory_path>_ - set directory as locale. It will force all crop, target, run and save commands to use this folder. You HAVE to end this with a "\"  
_-crop <filename>_ or _-c <filename>_ - put chosen file into "source". If Position or Size was specified, it will be used as position for a cut-out from that file. Result of that cutout will be put into "source"  
_-target <filename>_ or _-t <filename>_ - selects chosen file as initial canvas. This will be the drawing board. Right now it cannot be empty.  
_-search <turf>_ - searches for turf among the items. Purely for logging. Can be used to get the whitekey  
_-pos <x> <y>_ or _-p <x> <y>_ - indicate the position for next command that uses position (right now crop and make)  
_-size <sx> <sy>_ or _-s <sx> <sy>_ - indicate the size for next command that uses size (right now just crop)  
_-make_ or _-m_ - applies current "source" to "target" at indicated position  
_-save <filename>_ or _-s <filename>_ - save current "target" with all applied modifications  
_-key <mapkey>_ - set whitekey, i.e. what code should be ignored when copy-pasting the map. See search  
_-flush-target_ - clears current target  
_-flush-crop_ - clears current source  
_-newcode_ or _-nc_, _-newcode-_ or _-nc-" - sets or resets new code enforcement  
_-merge_ - forces app to ensure uniqueness of every map tile def. Will take more time to process with this on, but map will be cleaner  
_-log_ - turns logging on  
_>message_ - echoes message on the screen if logs are on  
_-newcmd <cmdname> <numparams> <cmdtext>_ - creates new command, use ##<number> as stand-ins for parameters  
_-cmd-<cmdname> <...>_ - runs a created command  
_-interactive_ - starts an interactive console where you can run all these commands in real time  
_-x_ - exit interactive console  

## Scripting

_-run <filename>_ or _-r <filename>_ - runs script. Scripts can run scripts and they will see eachothers created commands in order of calling  

Script language is tabulation controlled

Script is divided into 2 parts  
<codeblock>  
<choiceblock>

Choiceblock is started by putting a @ symbol infront of a choiceblock name  
Choiceblock consists of numbers that represent chance of something occuring, that is then followed by new script instance (that can have own codeblocks and choiceblocks)  

All codes are run before any choices are made, so if you want something to happen after code is run and all choices are made, make a choiceblock with a single non-zero option 

## Examples of Script

testscript.txt:

>-loc "Z:\Work\ColonialMarines\maps\"  
>-pos 90 70 -size 17 17 -crop Z.01.LV624.dmm  
>-save Z.01.LV624.hydro_crop.dmm  

testgeneration.txt

>//set next line to where the map folder is located, don't forget slash in the end  
>-loc "Z:\Work\ColonialMarines\maps\"  
>//uncomment this if you want to see logs  
>//-log  
>//don't change these lines  
>-target Z.01.LV624.dmm  
>-newcmd insert-LV624 3 "-crop Z.01.LV624.##1.dmm -pos ##2 ##3 -make"  
>//do the stuff here  
>//@... - is the chance block, don't use spaces here  
>@modify_medbay  
>	20  
>		-cmd-insert-LV624 medbay_normal 72 69  
>	40  
>		-cmd-insert-LV624 medbay_broken 72 69  
>	35  
>		-cmd-insert-LV624 medbay_lots_of_dead 72 69  
>	5  
>		-cmd-insert-LV624 medbay_last_stand 72 69  
>@modify_lz  
>	80  
>		-cmd-insert-LV624 LZ1_normal 145 25  
>		-cmd-insert-LV624 LZ2_normal 28 29  
>	20  
>		-cmd-insert-LV624 LZ1_actually_LZ2 145 25  
>		-cmd-insert-LV624 LZ2_actually_LZ1 28 29  
>@modify_cargo  
>	80  
>		-cmd-insert-LV624 cargo_normal 146 54  
>	20  
>		-cmd-insert-LV624 cargo_important 146 54  
>		//don't care about naming conventions but it's all up to you  
>		@modify_cargo_important  
>			80  
>				-cmd-insert-LV624 cargo_important_weapon_stashe 153 62  
>			15  
>				-cmd-insert-LV624 cargo_important_secret_research 153 62  
>			5  
>				-cmd-insert-LV624 cargo_important_yautja_tech 153 62  