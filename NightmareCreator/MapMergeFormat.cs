﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NightmareCreator
{
    public class MapMergeFormat : IMapFormat
    {
        public string Convert(DreamElement element)
        {
            var builder = new StringBuilder();
            builder.Append(element.ObjName);
            if (element.Parameters == null || element.Parameters.Count == 0)
                return builder.ToString();
            builder.Append("{\r\t");
            builder.Append(string.Join(";\r\t", element.Parameters.Select((kv, idx) => $"{kv.Key} = {kv.Value}")));
            builder.Append("\r\t}");

            return builder.ToString();
        }

        public string Convert(DreamMap map)
        {
            var sb = new StringBuilder();
            sb.AppendLine("//MAP CONVERTED BY NMC");
            sb.AppendJoin("\n", map.Tokens.Select((kv, index) => $"\"{kv.Key}\" = {kv.Value.ToDreamString(this)}"));
            sb.AppendLine("");

            for (var x = 0; x < map.Map.GetLength(1); x++)
            {
                var Z = map.ZLevel;
                var Y = 0;
                var X = x;
                var rowtext = new StringBuilder();
                for (var y = 0; y < map.Map.GetLength(0); y++)
                {
                    if (map.Map[y, x] == null)
                    {
                        Y = y;
                        continue;
                    }
                    rowtext.AppendLine(map.Map[y, x].Code);
                }
                sb.AppendLine($"({X + map.XOffset},{Y + map.YOffset},{map.ZLevel}) = " + "{\"");
                sb.Append(rowtext.ToString());
                sb.AppendLine("\"}");
            }


            return sb.ToString();
        }

        public string Convert(DreamToken token)
        {
            return "(\r" + string.Join(",\r", token.Contents.Select(x => x.ToDreamString(this))) + ")\r";
        }
    }
}
