﻿using Irony.Parsing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NightmareCreator
{
    [Language("Byond MAP", "1.0", "Byond map parser")]
    public class ByondMapGrammar : Grammar
    {
        public ByondMapGrammar() : base(false)
        {
            var lineComment = new CommentTerminal("line_comment", "//", "\n", "\r\n");
            NonGrammarTerminals.Add(lineComment);

            var number = TerminalFactory.CreateCSharpNumber("number");

            var string_literal = new StringLiteral("string", "\"", StringOptions.NoEscapes);
            var icon_literal = new StringLiteral("icon", "\'", StringOptions.NoEscapes);
            var string_huge_literal = new StringLiteral("string", "\"", StringOptions.AllowsLineBreak | StringOptions.NoEscapes);
            var Id_simple = TerminalFactory.CreatePythonIdentifier("id_simple");
            var slash = ToTerm("/");
            var comma = ToTerm(",");
            var semicolon = ToTerm(";");
            var key_list = ToTerm("list");
            var key_newlist = ToTerm("newlist");
            var key_null = ToTerm("null");



            //Non-terminals
            var any_list = new NonTerminal("any_list");
            var neg_number = new NonTerminal("neg_number");
            var def_type = new NonTerminal("type");
            var def_true_type = new NonTerminal("true_type");
            var def_full_id = new NonTerminal("full_id");
            var def_list = new NonTerminal("list_stuff");
            var def_list_above = new NonTerminal("list_above");
            var def_list_internal = new NonTerminal("list_internal");
            var def_value = new NonTerminal("value");
            var def_kv_pair = new NonTerminal("kv_pair");
            var def_obj_params = new NonTerminal("obj_params");
            var def_obj_params_or_empty = new NonTerminal("obj_params_or_empty");
            var def_obj_above = new NonTerminal("obj_above");
            var def_obj_param_item = new NonTerminal("obj_param_item");
            var def_obj_list = new NonTerminal("def_obj_list");
            var def_item_record = new NonTerminal("obj_item_record");
            var def_item_record_list = new NonTerminal("obj_item_record");

            var def_map_pos = new NonTerminal("map_pos");
            var def_map_pos_above = new NonTerminal("map_pos_above");
            var def_map_def = new NonTerminal("map_def");
            var def_map_def_list = new NonTerminal("def_map_def_list");

            var full_file = new NonTerminal("full_file");

            //BNF Rules
            this.Root = full_file;

            any_list.Rule = key_list | key_newlist;
            neg_number.Rule = "-" + number;
            def_type.Rule = MakePlusRule(def_type, slash, Id_simple);
            def_true_type.Rule = slash + def_type;
            def_full_id.Rule = Id_simple | string_literal;
            def_kv_pair.Rule = def_full_id + "=" + def_value;
            def_list_internal.Rule = def_kv_pair | def_value;
            def_list.Rule = MakeStarRule(def_list, comma, def_list_internal);
            def_list_above.Rule = any_list + "(" + def_list + ")";
            def_value.Rule = def_list_above | def_true_type | def_obj_above | string_literal | icon_literal | key_null | number | neg_number;
            def_obj_param_item.Rule = (Id_simple + "=" + def_value)|Empty;
            def_obj_params.Rule = MakePlusRule(def_obj_params, semicolon, def_obj_param_item);
            def_obj_params_or_empty.Rule = Empty | "{" + def_obj_params + "}";
            def_obj_above.Rule = def_true_type + def_obj_params_or_empty;
            def_obj_list.Rule = MakePlusRule(def_obj_list, comma, def_obj_above);
            def_item_record.Rule = string_literal + "=" + "(" + def_obj_list + ")";
            def_item_record_list.Rule = MakeStarRule(def_item_record_list, def_item_record);

            def_map_pos.Rule = MakePlusRule(def_map_pos, comma, number);
            def_map_pos_above.Rule = "(" + def_map_pos + ")";
            def_map_def.Rule = def_map_pos_above + "=" + "{" + string_huge_literal + "}";
            def_map_def_list.Rule = MakeStarRule(def_map_def_list, def_map_def);

            full_file.Rule = def_item_record_list + def_map_def_list;


            MarkPunctuation(",", ";", "(", ")", "{", "}","=","/");
            base.MarkTransient(def_obj_params_or_empty, def_value, def_map_pos_above, any_list, def_true_type);


        }//constructor
    }

    public static class ByondMapParserExtension
    {
        static Parser parser;
        static ByondMapParserExtension()
        {
            parser = new Parser(new ByondMapGrammar());
        }

        public static DreamMap FromString(string s)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";

            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            var dm = new DreamMap();


            parser.Parse(s);
            var ptree = parser.Context.CurrentParseTree;

            loadTokens(ptree.Root.ChildNodes.Where(x => x.Term.Name == "obj_item_record").FirstOrDefault(), dm);
            dm.TokSize = dm.Tokens.Max(x => x.Key.Length);
            loadMap(ptree.Root.ChildNodes.Where(x => x.Term.Name == "def_map_def_list").FirstOrDefault(), dm);

            return dm;
        }

        static void loadTokens(ParseTreeNode ptree, DreamMap dm)
        {
            var tokenlist = ptree.ChildNodes.Where(x => x.Term.Name == "obj_item_record")
                .Select(x => getToken(x)).ToDictionary(x => x.Code);
            dm.Tokens = tokenlist;
            foreach (var d in dm.Tokens)
            {
                var kfs = DreamMap.KeyFromString(d.Key);
                if (kfs > dm.BiggestKey)
                    dm.BiggestKey = kfs;
            }
        }
            

        static DreamToken getToken(ParseTreeNode ptree)
        {
            var dtok = new DreamToken();
            var tokname = ptree.ChildNodes.Where(x => x.Term.Name == "string").Select(x=>x.Token.ValueString).FirstOrDefault();
            var tokfill = ptree.ChildNodes.Where(x => x.Term.Name == "def_obj_list").FirstOrDefault();
            var elements = tokfill.ChildNodes.Select(x => getElement(x,tokname)).ToList();
            dtok.Code = tokname;
            dtok.Contents = elements;
            return dtok;
        }

        static DreamElement getElement(ParseTreeNode ptree, string tokname)
        {
            var element = new DreamElement();
            var type = ptree.ChildNodes.Where(x => x.Term.Name == "type").FirstOrDefault()?.ChildNodes.Select(x => x.Token.ValueString);
            element.ObjName = "/" + string.Join("/", type);
            var o_params = ptree.ChildNodes.Where(x => x.Term.Name == "obj_params").FirstOrDefault();
            if(o_params!=null)
            {
                var o_par_list = new Dictionary<string, string>();
                foreach (var kv in o_params.ChildNodes)
                {
                    if (kv.ChildNodes.Count == 0)
                        continue;
                    var x = kv.ChildNodes.Where(y => y.Term.Name == "Identifier").First().Token.ValueString;
                    var z = tokenToString(kv.ChildNodes.Where(y => y.Term.Name != "Identifier").FirstOrDefault());
                    if (o_par_list.ContainsKey(x))
                        Console.WriteLine("Already have field (" + x + ") in object (" + element.ObjName + ") in token " + tokname);
                    else
                        o_par_list.Add(x, z);
                }
                element.Parameters = o_par_list;
            }
            return element;//ptree.
        }

        static string tokenToString(ParseTreeNode ptree)
        {
            if (ptree == null)
                return "";
            if (ptree.Term.Name == "value")
                ptree = ptree.ChildNodes.First();
            switch(ptree.Term.Name)
            {
                case ("string"):
                    return "\"" + ptree.Token.ValueString + "\"";
                case ("number"):
                    return ptree.Token.ValueString;
                case ("neg_number"):
                    return "-" + ptree.ChildNodes.Where(x => x.Term.Name == "number").FirstOrDefault().Token.ValueString;
                case ("type"):
                    return "/" + string.Join("/", ptree.ChildNodes.Select(x => x.Token.ValueString));
                case ("null"):
                    return "null";
                case ("list_above"):
                    var listName = ptree.ChildNodes.Where(x => x.Term.Name != "list_stuff").FirstOrDefault()?.Token.ValueString;
                    var listNode = ptree.ChildNodes.Where(x => x.Term.Name == "list_stuff").FirstOrDefault()?.ChildNodes?.FirstOrDefault();
                    return listName+"(" +tokenToString(listNode)+")";
                case ("list_internal"):
                    var internals = ptree.ChildNodes.Select(x=> tokenToString(x));
                    return string.Join(",", internals);
                case ("icon"):
                    return "\'" + ptree.Token.ValueString + "\'";
                default:
                    return "";
            }
        }

        class MapNode
        {
            public int[] InsertAt { get; set; }
            public string Value { get; set; }
            public int Col_Count { get; set; }
            public int Row_Count { get; set; }
            public int X { get; set; }
            public int Y { get; set; }
            public int Z { get; set; }
        }

        static void loadMap(ParseTreeNode ptree, DreamMap dm)
        {
            var mapDefs = ptree.ChildNodes.Where(x => x.Term.Name == "map_def").Select(x =>
                new MapNode()
                {
                    InsertAt = x.ChildNodes.Where(y => y.Term.Name == "map_pos").FirstOrDefault().ChildNodes.Select(z => int.Parse(z.Token.ValueString)).ToArray(),
                    Value = x.ChildNodes.Where(y => y.Term.Name == "string").FirstOrDefault().Token.ValueString
                }).ToArray();
            dm.XOffset = int.MaxValue;
            dm.YOffset = int.MaxValue;
            dm.ZLevel = 0;

            var maxX = 0;
            var maxY = 0;
            foreach (var def in mapDefs)
            {
                def.Row_Count = def.Value.Trim().Count(x => x == '\r') + 1;
                def.Col_Count = def.Value.Trim().Split(new char[] { '\r', '\n' },StringSplitOptions.RemoveEmptyEntries).Select(x => x.Trim().Length / dm.TokSize).Max();
                def.X = def.InsertAt[0];
                def.Y = def.InsertAt[1];
                def.Z = def.InsertAt[2];
                if (def.X < dm.XOffset)
                    dm.XOffset = def.X;
                if (def.Y < dm.YOffset)
                    dm.YOffset = def.Y;
                if (def.Z != dm.ZLevel)
                    if (dm.ZLevel > 0)
                        throw new Exception("Multi Z-level single file maps are not supported.");
                    else
                        dm.ZLevel = def.Z;
                if (def.X + def.Col_Count > maxX)
                    maxX = def.X + def.Col_Count;
                if (def.Y + def.Row_Count > maxY)
                    maxY = def.Y + def.Row_Count;
            }

            dm.Map = new DreamToken[maxY - dm.YOffset, maxX - dm.XOffset];

            foreach (var def in mapDefs)
            {
                var offx = def.X - dm.XOffset;
                var offy = def.Y - dm.YOffset;
                var tks = def.Value.Trim().Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
                for (var Y = 0; Y < def.Row_Count; Y++)
                {
                    var tk = tks[Y].Trim();
                    for (var X = 0; X < def.Col_Count; X++)
                    {
                        var subs = tk.Substring(X * dm.TokSize, dm.TokSize);
                        var tok = dm.Tokens[subs];
                        dm.Map[Y+ offy, X+offx] = tok;
                        tok.Usages++;
                    }
                }
            }
        }

        
    }
}
