﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NightmareCreator
{
    public interface IMapFormat
    {
        string Convert(DreamElement element);
        string Convert(DreamMap element);
        string Convert(DreamToken element);
    }
}
