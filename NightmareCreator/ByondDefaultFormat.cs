﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NightmareCreator
{
    public class ByondDefaultFormat : IMapFormat
    {
        public string Convert(DreamElement element)
        {
            var builder = new StringBuilder();
            builder.Append(element.ObjName);
            if (element.Parameters == null || element.Parameters.Count == 0)
                return builder.ToString();
            builder.Append("{");
            builder.Append(string.Join(";", element.Parameters.Select((kv, idx) => $"{kv.Key} = {kv.Value}")));
            builder.Append("}");

            return builder.ToString();
        }

        public string Convert(DreamMap map)
        {
            var sb = new StringBuilder();
            sb.AppendJoin("\n", map.Tokens.Select((kv, index) => $"\"{kv.Key}\" = {kv.Value.ToDreamString(this)}"));
            sb.AppendLine("");

            sb.AppendLine($"({map.XOffset},{map.YOffset},{map.ZLevel}) = " + "{\"");

            for (var y = 0; y < map.Map.GetLength(0); y++)
            {
                var rowtext = new StringBuilder();
                for (var x = 0; x < map.Map.GetLength(1); x++)
                {
                    if (map.Map[y, x] == null)
                    {
                        continue;
                    }
                    rowtext.Append(map.Map[y, x].Code);
                }
                
                sb.AppendLine(rowtext.ToString());
                
            }
            sb.AppendLine("\"}");

            return sb.ToString();
        }

        public string Convert(DreamToken token)
        {
            return "(" + string.Join(",", token.Contents.Select(x => x.ToDreamString(this))) + ")";
        }
    }
}
