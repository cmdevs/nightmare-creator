﻿using System;
using System.IO;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;

namespace NightmareCreator
{
    class Program
    {
        static Stack<NightmareState> StateStack = new Stack<NightmareState>();
        static List<NightmareCommand> Commands = DefaultCommands.GenerateDefaultCommands();
        public static Dictionary<string, IMapFormat> MapFormats = new Dictionary<string, IMapFormat>();

        static Program()
        {
            MapFormats.Add("MapMerge", new MapMergeFormat());
            MapFormats.Add("ByondDefault", new ByondDefaultFormat());
            var state = new NightmareState();
            state.MapFormat = MapFormats["ByondDefault"];
            StateStack.Push(state);
        }

        public static int Main(string[] args)
        {
            if (args.Length == 0)
            {
                return -1;
            }

            if (args[0].StartsWith("//"))
                return 0;

            StateStack.Peek().NextCommandToExecute = new Stack<NightmareCommand>();
            StateStack.Peek().ExecutionParameterPosition = new Stack<int>();
            StateStack.Peek().QuoteMode = false;
            StateStack.Peek().SupersafeMode = false;
            try
            {
                for (var i = 0; i < args.Length; i++)
                {

                    var state = StateStack.Peek();
                    if (args[i].StartsWith('"') && !args[i].StartsWith("\\\"") && !state.QuoteMode)
                    {
                        if (args[i].EndsWith('"') && !args[i].EndsWith("\\\""))
                        {
                            state.Parameters.Add(args[i].TrimStart('"').TrimEnd('"').Replace("\\\"", "\""));
                        }
                        else
                        {
                            state.Parameters.Add(args[i].TrimStart('"').Replace("\\\"", "\""));
                            state.QuoteMode = true;
                        }
                        continue;
                    }
                    if (state.QuoteMode)
                    {
                        var ct = state.Parameters.Count - 1;
                        if (args[i].EndsWith('"') && !args[i].EndsWith("\\\""))
                        {
                            var trm = args[i].TrimEnd('"');
                            state.Parameters[ct] = state.Parameters[ct] + (string.IsNullOrEmpty(trm) ? "" : (" " + trm));
                            state.QuoteMode = false;
                        }
                        else
                            state.Parameters[ct] = state.Parameters[ct] + " " + args[i];
                        state.Parameters[ct] = state.Parameters[ct].Replace("\\\"", "\"");
                        continue;
                    }

                    var matched = Commands.Where(x =>
                        (x.Inlined && args[i].StartsWith(x.Command)) ||
                        (!x.Inlined && args[i] == x.Command)
                    );

                    var matchCount = matched.Count();

                    if(args[i]=="--")
                    {
                        RunFunction(state, true);
                        continue;
                    }

                    if (matchCount == 0)
                    {
                        state.Parameters.Add(args[i]);
                        var parcntold = state.ExecutionParameterPosition.Count > 0 ? state.ExecutionParameterPosition.Peek() : 0;
                        var peekf = state.NextCommandToExecute.Count > 0 ? state.NextCommandToExecute.Peek() : null;
                        if (peekf!=null && peekf.PersistantParameters && peekf.AllowedLengths.Length == 1 && peekf.AllowedLengths[0] == state.Parameters.Count - parcntold)
                        {
                            RunFunction(state, true);
                        }
                        continue;
                    }

                    if (matchCount > 1)
                    {
                        Console.WriteLine("fail");
                        return 0;
                    }

                    var cmd = matched.FirstOrDefault();
                    
                    if (!cmd.PersistantParameters)
                        RunFunction(state);

                    if (state.ExitNow)
                        return -1;

                    if (!cmd.PersistantParameters)
                        RunFunction(state);

                    state.NextCommandToExecute.Push(cmd);
                    state.ExecutionParameterPosition.Push(state.Parameters.Count);


                    if (cmd.Inlined)
                    {
                        var fcmd = args[i].ReplaceFirst(cmd.Command, "");
                        if (fcmd.StartsWith('"'))
                        {
                            state.QuoteMode = true;
                            fcmd = fcmd.TrimStart('"');
                        }
                        state.Parameters.Add(fcmd);
                        var parcntold = state.ExecutionParameterPosition.Count > 0 ? state.ExecutionParameterPosition.Peek() : 0;
                        var peekf = state.NextCommandToExecute.Count > 0 ? state.NextCommandToExecute.Peek() : null;
                        if (peekf != null && peekf.PersistantParameters && peekf.AllowedLengths.Length == 1 && peekf.AllowedLengths[0] == state.Parameters.Count - parcntold)
                        {
                            RunFunction(state, true);
                        }
                    }
                }
                RunFunction(StateStack.Peek());
            }
            catch (ArgumentNullException e)
            {
                if (StateStack.Peek().Unsafe)
                {
                    DefaultCommands.ConsoleLog(">>RUNTIME EXCEPTION DURING UNSAFE MODE. LINE ABORTED.");
                    DefaultCommands.ConsoleLog(e.Message);
                    return 0;
                }
                throw;
            }
            return 0;
        }

        static void RunFunction(NightmareState state, bool pop_once = false)
        {
            try
            {
                while (state.NextCommandToExecute.Count > 0)
                {
                    var func = state.NextCommandToExecute.Pop();
                    var parc = state.ExecutionParameterPosition.Pop();
                    var parameters = state.Parameters.Skip(parc).ToArray();
                    if (func == null)
                        return;
                    if (!func.Supersafe && state.SupersafeMode)
                        if (state.Logging)
                        {
                            Console.WriteLine(">>Supersafe mode on, can't run " + func.Name);
                            return;
                        }

                    if (func.AllowedLengths.Count() != 0 && !func.AllowedLengths.Contains(parameters.Length))
                    {
                        if (state.Logging)
                            Console.WriteLine(">>Wrong parameter number, function " + func.Name);
                        return;
                    }

                    func.Action(state, parameters);
                    if (pop_once)
                        return;
                }
            }
            finally
            {
                if (!pop_once)
                {
                    state.NextCommandToExecute.Clear();
                    state.ExecutionParameterPosition.Clear();
                    state.Parameters.Clear();
                }
            }
        }
    }
}
