﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace NightmareCreator
{
    public class DreamElement
    {
        public string ObjName { get; set; }
        public Dictionary<string,string> Parameters { get; set; }

        public string ToDreamString(IMapFormat format)
        {
            return format.Convert(this);
        }

        public static DreamElement FromString(string s)
        {
            var DE = new DreamElement();
            DE.Parameters = new Dictionary<string, string>();

            var objNameStr = new StringBuilder();
            var paramNameStr = new StringBuilder();
            var valueStr = new StringBuilder();
            var mode = 0;

            for(var i=0; i<s.Length; i++)
            {
                if (s[i] == '(' && mode > 0 && i - 5 > 0 && s.Substring(i - 4, 4).ToLower() == "list")
                    mode = 10;

                if (s[i] == '{' && mode <3)
                {
                    mode = 1;
                    DE.ObjName = objNameStr.ToString();
                    objNameStr.Clear();
                }
                if (s[i] == '=' && mode < 3)
                    mode = 2;
                if (s[i] == ';' && mode < 3)
                {
                    mode = 1;
                    DE.Parameters.Add(paramNameStr.ToString(), valueStr.ToString());
                    paramNameStr.Clear();
                    valueStr.Clear();
                }
                if (s[i] == '}' && mode > 0 && mode < 3)
                {
                    mode = 1;
                    DE.Parameters.Add(paramNameStr.ToString(), valueStr.ToString());
                    paramNameStr.Clear();
                    valueStr.Clear();
                }
                if (s[i] == '\"' && mode > 1 && mode<10 && s[i-1]!='\\')
                    mode = mode==2?3:2;
                if ("{}=; ".IndexOf(s[i])>-1 && mode < 3)
                    continue;

                if (mode == 0)
                    objNameStr.Append(s[i]);
                if (mode == 1)
                    paramNameStr.Append(s[i]);
                if (mode > 1)
                    valueStr.Append(s[i]);

                if (s[i] == ')' && mode == 10)
                    mode = 2;

            }
            //flush if no params
            if(mode==0)
                DE.ObjName = objNameStr.ToString();
            return DE;
        }

        //public override string ToString()
        //{
        //    return ToDreamString();
        //}
    }
}
