﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NightmareCreator
{
    public delegate void NightmareFunction(NightmareState state, IList<string> parameters);
    public class NightmareCommand
    {
        public string Command { get; set; }
        public int[] AllowedLengths { get; set; }
        public bool Inlined { get; set; } //first parameter is -CMD-<this>
        public bool PersistantParameters { get; set; } //Stack up parameters
        public string Help { get; set; }
        public string Name { get; set; }
        public NightmareFunction Action { get; set; }
        public bool Supersafe { get; set; }
    }
}
