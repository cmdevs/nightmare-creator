﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NightmareCreator
{
    public class DreamToken
    {
        public List<DreamElement> Contents { get; set; }

        public int Usages { get; set; }
        public string Code { get; set; }

        public string ToDreamString(IMapFormat format)
        {
            return format.Convert(this);
        }

        public static DreamToken FromString(string s)
        {
            var DT = new DreamToken();
            DT.Contents = new List<DreamElement>();

            var valueStr = new StringBuilder();
            var mode = 0;

            s=s.Trim();

            for (var i = 0; i < s.Length; i++)
            {
                if (s[i] == '(' && mode > 0 && i-5>0 && s.Substring(i - 4, 4).ToLower() == "list")
                    mode = 3;

                if (s[i] == '(' && mode == 0)
                    mode = 1;

                if (s[i] == '\"' && mode > 0 && mode !=3)
                    mode = mode == 1 ? 2 : 1;
                if (s[i] == ',' && mode == 1)
                {
                    DT.Contents.Add(DreamElement.FromString(valueStr.ToString()));
                    valueStr.Clear();
                }

                if (s[i] == ')' && mode == 1)
                {
                    DT.Contents.Add(DreamElement.FromString(valueStr.ToString()));
                    valueStr.Clear();
                }                

                if ("(),".IndexOf(s[i]) > -1 && mode < 2)
                    continue;

                if (mode > 0)
                    valueStr.Append(s[i]);

                if (s[i] == ')' && mode == 3)
                    mode = 1;
            }

            return DT;
        }

        //public override string ToString()
        //{
        //    return ToDreamString();
        //}

        public DreamToken Clone()
        {
            var DreamToken = new DreamToken();
            DreamToken.Code = Code;
            DreamToken.Contents = new List<DreamElement>(Contents);
            return DreamToken;
        }
    }
}
