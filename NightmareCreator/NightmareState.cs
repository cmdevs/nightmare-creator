﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace NightmareCreator
{
    public class NightmareState
    {
        public Dictionary<string, string> Command { get; set; } = new Dictionary<string, string>();
        public Dictionary<string, int> CommandSize { get; set; } = new Dictionary<string, int>();
        public Dictionary<string, string> Variables { get; set; } = new Dictionary<string, string>();
        public DreamMap Crop { get; set; } = null;
        public DreamMap Target { get; set; } = null;
        public Stack<DreamMap> TargetStack { get; set; } = new Stack<DreamMap>();
        public bool GenerateCodes { get; set; } = false;
        public Point Point { get; set; }
        public Size Size { get; set; }
        public string Locale { get; set; }
        public bool Logging { get; set; } = false;
        public bool Merge { get; set; } = false;
        public string Whitekey { get; set; }
        public string Scriptlocale { get; set; }
        public List<string> Parameters { get; set; } = new List<string>();
        public Stack<NightmareCommand> NextCommandToExecute { get; set; } = new Stack<NightmareCommand>();
        public Stack<int> ExecutionParameterPosition { get; set; } = new Stack<int>();
        public bool ExitNow { get; set; } = false;
        public bool QuoteMode { get; set; }
        public bool Unsafe { get; set; }
        public bool SupersafeMode { get; set; }
        public IMapFormat MapFormat { get; set; }
    }
}
