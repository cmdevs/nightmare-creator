﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace NightmareCreator
{
    public class DreamMap
    {
        public Dictionary<string, DreamToken> Tokens { get; set; } = new Dictionary<string, DreamToken>();

        public int TokSize { get; set; }

        public DreamToken[,] Map { get; set; }

        public int BiggestKey { get; set; }

        public int ZLevel { get; set; }
        public int XOffset { get; set; }
        public int YOffset { get; set; }

        public string ToDreamString(IMapFormat format)
        {
            return format.Convert(this);
        }

        public static DreamMap FromString(string s)
        {
            return ByondMapParserExtension.FromString(s);
        }

        public string MapString()
        {
            var sb = new StringBuilder();
            for (var i = 0; i < Map.GetLength(0); i++)
            {
                for (var k = 0; k < Map.GetLength(1); k++)
                    sb.Append(Map[i, k].Code[2]);
                sb.AppendLine();
            }
            return sb.ToString();
        }

        public DreamMap SubMap(Point start, Size size, bool generateNew = false)
        {
            var DM = new DreamMap();
            DM.Tokens = new Dictionary<string, DreamToken>();
            DM.Map = new DreamToken[size.Height, size.Width];

            var ky = 0;
            var tokenMapping = new Dictionary<string, DreamToken>();

            for(var i = 0; i<size.Height;i++)
                for (var k = 0; k < size.Width; k++)
                {
                    var element = Map[start.Y + i, start.X + k];
                    if (!tokenMapping.ContainsKey(element.Code))
                    {
                        var key = generateNew?GenerateKey(ky, TokSize) : element.Code;
                        element = element.Clone();
                        tokenMapping.Add(element.Code, element);
                        element.Code = key;                       
                        DM.Tokens.Add(key, element);
                        ky++;

                        DM.Map[i, k] = element;
                    }
                    else
                    {
                        DM.Map[i, k] = tokenMapping[element.Code];
                    }
                }
            DM.BiggestKey = generateNew ? ky : BiggestKey;
            return DM;
        }

        private static string genstr = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        public static string GenerateKey(int i, int genlen)
        {
            var alphalength = genstr.Length;
            var que = new Stack<char>();
            for (var x = 0; x < genlen; x++)
            {
                que.Push(genstr[i % alphalength]);
                i = i / alphalength;
            }

            return new string(que.ToArray());
        }

        public static int KeyFromString(string str)
        {
            var alphalength = genstr.Length;
            var s = 0;
            for (var i = 0; i < str.Length; i++)
            {
                s += genstr.IndexOf(str[i]);
                if (i == str.Length - 1)
                    continue;
                s *= alphalength;
            }
            return s;
        }

        public void InjectMap(DreamMap inject, Point start, string ownKey=null, string injectKey=null, bool splice=false, bool checkUnbinded=false)
        {
            var defaultSk = new ByondDefaultFormat();
            validateBiggest();
            var injHeight = inject.Map.GetLength(0);
            var injWidth = inject.Map.GetLength(1);

            var spliceDictionary = new Dictionary<string, DreamToken>();
            if (ownKey != null)
                ownKey = GenerateKey(KeyFromString(ownKey), TokSize);
            var ownToken = (ownKey!=null&&Tokens.ContainsKey(ownKey)) ? Tokens[ownKey] : (DreamToken)null;
            if (injectKey != null)
                injectKey = GenerateKey(KeyFromString(injectKey), inject.TokSize);
            var otherToken = (injectKey != null && inject.Tokens.ContainsKey(injectKey)) ? inject.Tokens[injectKey] : (DreamToken)null;

            var keycount_inject = inject.Tokens.Keys.Count;

            if (BiggestKey + keycount_inject > Math.Pow(genstr.Length, TokSize))
                Rebase(Convert.ToInt32(Math.Ceiling(Math.Log(BiggestKey + keycount_inject, genstr.Length))));

            foreach (var kv in inject.Tokens)
            {
                var token = kv.Value;
                var key = kv.Key;
                if (token == otherToken)
                    continue;

                DreamToken splc = null;
                if (splice)
                {
                    splc = Tokens.FirstOrDefault(x => x.Value.ToDreamString(defaultSk) == token.ToDreamString(defaultSk)).Value;
                }
                if (splc != null)
                    spliceDictionary.Add(key, splc);
                else
                {
                    BiggestKey++;
                    var nky = GenerateKey(BiggestKey, TokSize);
                    var cln = token.Clone();
                    Tokens.Add(nky, cln);
                    spliceDictionary.Add(token.Code, cln);
                    cln.Code = nky;
                }
            }

            for(var y = 0; y < injHeight; y++)
                for (var x = 0; x < injWidth; x++)
                {
                    var oldcode = inject.Map[y, x].Code;
                    var currentcode = Map[start.Y + y, start.X + x].Code;

                    if((oldcode != injectKey) && (currentcode == ownKey || ownKey==null))
                        Map[start.Y + y, start.X + x] = spliceDictionary[oldcode];
                }
        }

        public void Rebase(int newkeylen)
        {
            BiggestKey = 0;
            TokSize = newkeylen;
            foreach (var kv in Tokens)
            {
                kv.Value.Code = GenerateKey(BiggestKey, TokSize);
                BiggestKey++;
            }
            var newdict = Tokens.ToDictionary(x => x.Value.Code, x=>x.Value);
            Tokens = newdict;
        }

        private void validateBiggest()
        {
            BiggestKey = 0;
            foreach (var kv in Tokens)
            {
                kv.Value.Code = kv.Key;
                var key = KeyFromString(kv.Value.Code);
                if (key > BiggestKey)
                    BiggestKey = key;
            }
        }
    }
}
