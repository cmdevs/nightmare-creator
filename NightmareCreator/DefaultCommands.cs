﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

namespace NightmareCreator
{
    public delegate void LoggingFunction(string str);

    public delegate int RunningFunction(List<string> str);

    public static class DefaultCommands
    {
        public static List<NightmareCommand> GenerateDefaultCommands()
        {
            var result = new List<NightmareCommand>();
            #region Commands
            result.Add(new NightmareCommand
            {
                Command = "-crop",
                Action = FCrop,
                AllowedLengths = new int[] {1},
                Inlined = false,
                PersistantParameters = false,
                Name = "Crop",
                Help = "Crop Help"
            });

            result.Add(new NightmareCommand
            {
                Command = "-target",
                Action = FTarget,
                AllowedLengths = new int[] { 1 },
                Inlined = false,
                PersistantParameters = false,
                Name = "Target",
                Help = "Target Help"
            });

            result.Add(new NightmareCommand
            {
                Command = "-run",
                Action = FConfigRun,
                AllowedLengths = new int[] { 1 },
                Inlined = false,
                PersistantParameters = false,
                Name = "Run Config",
                Help = "Run Config Help"
            });

            result.Add(new NightmareCommand
            {
                Command = "-search",
                Action = FSearch,
                AllowedLengths = new int[] { 1 },
                Inlined = false,
                PersistantParameters = true,
                Name = "Search Object",
                Help = "Search Object Help"
            });

            result.Add(new NightmareCommand
            {
                Command = "-search-crop",
                Action = FSearchCrop,
                AllowedLengths = new int[] { 1 },
                Inlined = false,
                PersistantParameters = true,
                Name = "Search Crop",
                Help = "Search Crop Help"
            });

            result.Add(new NightmareCommand
            {
                Command = "-newcode",
                Action = FGenCodes,
                AllowedLengths = new int[] { 0 },
                Inlined = false,
                PersistantParameters = false,
                Name = "Force Generate New Codes",
                Help = "Force Generate New Codes Help"
            });

            result.Add(new NightmareCommand
            {
                Command = "-pos",
                Action = FPoint,
                AllowedLengths = new int[] { 1, 2 },
                Inlined = false,
                PersistantParameters = false,
                Name = "Set Position",
                Help = "Set Position Help"
            });

            result.Add(new NightmareCommand
            {
                Command = "-size",
                Action = FSize,
                AllowedLengths = new int[] { 1, 2 },
                Inlined = false,
                PersistantParameters = false,
                Name = "Set Size",
                Help = "Set Size Help"
            });

            result.Add(new NightmareCommand
            {
                Command = "-loc",
                Action = FLocale,
                AllowedLengths = new int[] { 1 },
                Inlined = false,
                PersistantParameters = false,
                Name = "Set Locale",
                Help = "Set Locale Help"
            });

            result.Add(new NightmareCommand
            {
                Command = "-newcode-off",
                Action = FGenCodesOff,
                AllowedLengths = new int[] { 0 },
                Inlined = false,
                PersistantParameters = false,
                Name = "Force Generate New Codes Off",
                Help = "Force Generate New Codes Off Help"
            });

            result.Add(new NightmareCommand
            {
                Command = "-log",
                Action = FLogging,
                AllowedLengths = new int[] { 0 },
                Inlined = false,
                PersistantParameters = false,
                Name = "Logs",
                Help = "Logs Help"
            });

            result.Add(new NightmareCommand
            {
                Command = "-log-off",
                Action = FLoggingOff,
                AllowedLengths = new int[] { 0 },
                Inlined = false,
                PersistantParameters = false,
                Name = "Logs Off",
                Help = "Logs Off Help"
            });

            result.Add(new NightmareCommand
            {
                Command = "-save",
                Action = FSave,
                AllowedLengths = new int[] { 1 },
                Inlined = false,
                PersistantParameters = false,
                Name = "Save File",
                Help = "Save File Help"
            });

            result.Add(new NightmareCommand
            {
                Command = "-make",
                Action = FMake,
                AllowedLengths = new int[] { 0 },
                Inlined = false,
                PersistantParameters = false,
                Name = "Make Changes",
                Help = "Make Changes Help"
            });

            result.Add(new NightmareCommand
            {
                Command = "-interactive",
                Action = FRun,
                AllowedLengths = new int[] { 0 },
                Inlined = false,
                PersistantParameters = false,
                Name = "Interactive Mode",
                Help = "Interactive Mode Help"
            });

            result.Add(new NightmareCommand
            {
                Command = "-key",
                Action = FWhiteKey,
                AllowedLengths = new int[] { 1 },
                Inlined = false,
                PersistantParameters = false,
                Name = "Set Whitekey",
                Help = "Set Whitekey Help"
            });

            result.Add(new NightmareCommand
            {
                Command = "-key-off",
                Action = FClearWhitekey,
                AllowedLengths = new int[] { 0 },
                Inlined = false,
                PersistantParameters = false,
                Name = "Clear Whitekey",
                Help = "Clear Whitekey Help"
            });

            result.Add(new NightmareCommand
            {
                Command = "-flush-key",
                Action = FClearWhitekey,
                AllowedLengths = new int[] { 0 },
                Inlined = false,
                PersistantParameters = false,
                Name = "Clear Whitekey",
                Help = "Clear Whitekey Help"
            });

            result.Add(new NightmareCommand
            {
                Command = "-merge",
                Action = FMerge,
                AllowedLengths = new int[] { 0 },
                Inlined = false,
                PersistantParameters = false,
                Name = "Set Merge",
                Help = "Set Merge Help"
            });

            result.Add(new NightmareCommand
            {
                Command = "-merge-off",
                Action = FMergeOff,
                AllowedLengths = new int[] { 0 },
                Inlined = false,
                PersistantParameters = false,
                Name = "Clear Merge",
                Help = "Clear Merge Help"
            });

            result.Add(new NightmareCommand
            {
                Command = ">",
                Action = FLog,
                AllowedLengths = new int[] { 1 },
                Inlined = true,
                PersistantParameters = false,
                Name = "Log",
                Help = "Log Help"
            });

            result.Add(new NightmareCommand
            {
                Command = "-echo",
                Action = FEcho,
                AllowedLengths = new int[] { 1 },
                Inlined = false,
                PersistantParameters = false,
                Name = "Echo",
                Help = "Echo Help"
            });

            result.Add(new NightmareCommand
            {
                Command = "-newcmd",
                Action = FNewCommand,
                AllowedLengths = new int[] { 3 },
                Inlined = false,
                PersistantParameters = false,
                Name = "New Command",
                Help = "New Command Help"
            });

            result.Add(new NightmareCommand
            {
                Command = "-cmd-",
                Action = FCallCommand,
                AllowedLengths = new int[] { },
                Inlined = true,
                PersistantParameters = false,
                Name = "Call Command",
                Help = "Call Command Help"
            });

            result.Add(new NightmareCommand
            {
                Command = "-flush-target",
                Action = FClearTarget,
                AllowedLengths = new int[] { 0 },
                Inlined = false,
                PersistantParameters = false,
                Name = "Clear Target",
                Help = "Clear Target Help"
            });

            result.Add(new NightmareCommand
            {
                Command = "-flush-target",
                Action = FClearTarget,
                AllowedLengths = new int[] { 0 },
                Inlined = false,
                PersistantParameters = false,
                Name = "Clear Target",
                Help = "Clear Target Help"
            });

            result.Add(new NightmareCommand
            {
                Command = "-flush-crop",
                Action = FClearCrop,
                AllowedLengths = new int[] { 0 },
                Inlined = false,
                PersistantParameters = false,
                Name = "Clear Crop",
                Help = "Clear Crop Help"
            });

            result.Add(new NightmareCommand
            {
                Command = "-loc-scripts",
                Action = FScriptLocale,
                AllowedLengths = new int[] { 1 },
                Inlined = false,
                PersistantParameters = false,
                Name = "Set Script Locale",
                Help = "Set Script Locale Help"
            });

            result.Add(new NightmareCommand
            {
                Command = "-kill-obj",
                Action = FRemoveObject,
                AllowedLengths = new int[] { 1 },
                Inlined = false,
                PersistantParameters = false,
                Name = "Kill Objects",
                Help = "Kill Objects Help"
            });

            result.Add(new NightmareCommand
            {
                Command = "-kill-turf",
                Action = FRemoveTurf,
                AllowedLengths = new int[] { 1 },
                Inlined = false,
                PersistantParameters = false,
                Name = "Kill Turf",
                Help = "Kill Turf Help"
            });

            result.Add(new NightmareCommand
            {
                Command = "-new-map",
                Action = FEmptyMap,
                AllowedLengths = new int[] { 1 },
                Inlined = false,
                PersistantParameters = false,
                Name = "Empty Map",
                Help = "Empty Map Help"
            });

            result.Add(new NightmareCommand
            {
                Command = "-swap",
                Action = FSwap,
                AllowedLengths = new int[] { 0 },
                Inlined = false,
                PersistantParameters = false,
                Name = "Swap",
                Help = "Swap Help"
            });

            result.Add(new NightmareCommand
            {
                Command = "-set-",
                Action = FSetVar,
                AllowedLengths = new int[] { 2 },
                Inlined = true,
                PersistantParameters = false,
                Supersafe = true,
                Name = "Set Var",
                Help = "Set Var Help"
            });

            result.Add(new NightmareCommand
            {
                Command = "-get-",
                Action = FGetVar,
                AllowedLengths = new int[] { 1 },
                Inlined = true,
                PersistantParameters = true,
                Name = "Get Var",
                Help = "Get Var Help"
            });

            result.Add(new NightmareCommand
            {
                Command = "-set",
                Action = FSetVar,
                AllowedLengths = new int[] { 2 },
                Inlined = false,
                PersistantParameters = false,
                Supersafe = true,
                Name = "Set Var",
                Help = "Set Var Help"
            });

            result.Add(new NightmareCommand
            {
                Command = "-get",
                Action = FGetVar,
                AllowedLengths = new int[] { 1 },
                Inlined = false,
                PersistantParameters = true,
                Name = "Get Var",
                Help = "Get Var Help"
            });

            result.Add(new NightmareCommand
            {
                Command = "-unset-",
                Action = FUnsetVar,
                AllowedLengths = new int[] { 1 },
                Inlined = true,
                PersistantParameters = false,
                Name = "Unset Var",
                Help = "Unset Var Help"
            });

            result.Add(new NightmareCommand
            {
                Command = "-unset",
                Action = FUnsetVar,
                AllowedLengths = new int[] { 1 },
                Inlined = false,
                PersistantParameters = false,
                Name = "Unset Var",
                Help = "Unset Var Help"
            });

            result.Add(new NightmareCommand
            {
                Command = "-map-locate",
                Action = FMapLocate,
                AllowedLengths = new int[] { 1, 2, 3, 4 },
                Inlined = false,
                PersistantParameters = true,
                Name = "Locate on Map",
                Help = "Locate on Map Help"
            });

            result.Add(new NightmareCommand
            {
                Command = "-unsafe-mode",
                Action = FUnsafe,
                AllowedLengths = new int[] { 0 },
                Inlined = false,
                PersistantParameters = false,
                Name = "Unsafe Mode",
                Help = "Unsafe Mode Help"
            });

            result.Add(new NightmareCommand
            {
                Command = "-supersafe-mode",
                Action = FSupersafe,
                AllowedLengths = new int[] { 0 },
                Inlined = false,
                PersistantParameters = false,
                Name = "Supersafe Mode",
                Help = "Supersafe Mode Help"
            });

            result.Add(new NightmareCommand
            {
                Command = "-dist",
                Action = FDistance,
                AllowedLengths = new int[] { 1, 2},
                Inlined = false,
                PersistantParameters = true,
                Name = "Get Distance",
                Help = "Get Distance Help"
            });

            result.Add(new NightmareCommand
            {
                Command = "-ccat",
                Action = FConcat,
                AllowedLengths = new int[] { },
                Inlined = false,
                PersistantParameters = true,
                Name = "Concat",
                Help = "Concat Help"
            });

            result.Add(new NightmareCommand
            {
                Command = "-clear-vars",
                Action = FClearVars,
                AllowedLengths = new int[] { 0 },
                Inlined = false,
                PersistantParameters = false,
                Name = "Clear Variables",
                Help = "Clear Variables Help"
            });

            result.Add(new NightmareCommand
            {
                Command = "-rebase",
                Action = FRebase,
                AllowedLengths = new int[] { 0 },
                Inlined = false,
                PersistantParameters = false,
                Name = "Rebase",
                Help = "Rebase Help"
            });

            result.Add(new NightmareCommand
            {
                Command = "-format",
                Action = FSetFormat,
                AllowedLengths = new int[] { 1 },
                Inlined = false,
                PersistantParameters = false,
                Name = "Set Format",
                Help = "Set Format Help"
            });

            result.Add(new NightmareCommand
            {
                Command = "-transform",
                Action = FTransform,
                AllowedLengths = new int[] { 1 },
                Inlined = false,
                PersistantParameters = false,
                Name = "Transform",
                Help = "Transform Help"
            });

            result.Add(new NightmareCommand
            {
                Command = "-objectdefault",
                Action = FObjectDefault,
                AllowedLengths = new int[] { 3 },
                Inlined = false,
                PersistantParameters = false,
                Name = "Set Object Default",
                Help = "Set Object Default Help"
            });

            result.Add(new NightmareCommand
            {
                Command = "-rotatedir",
                Action = FRotateDir,
                AllowedLengths = new int[] { 2 },
                Inlined = false,
                PersistantParameters = false,
                Name = "Rotate Objects",
                Help = "Rotate Objects Help"
            });

            result.Add(new NightmareCommand
            {
                Command = "-custom",
                Action = FCustom2,
                AllowedLengths = new int[] { },
                Inlined = false,
                PersistantParameters = false,
                Name = "Custom",
                Help = "Custom Help"
            });

            #endregion
            return result;
        }

        public static LoggingFunction ConsoleLog { get; set; } = (x) => { Console.WriteLine(x); };

        public static RunningFunction Run { get; set; } = (x) => { return Program.Main(x.ToArray()); };

        #region Functions
        static void FCustom(NightmareState state, IList<string> parameters)
        {
            var root = parameters[0];
            var max_n = parameters.Count;

            var dest_lst = new List<string>();
            for (var i = 1; i < max_n; i++)
            {
                var dmCropS = File.ReadAllText(state.Locale + parameters[i]);
                var dmCrop = DreamMap.FromString(dmCropS);
                dest_lst.AddRange(dmCrop.Tokens.SelectMany(x => x.Value.Contents.Select(y => y.ObjName)).Distinct());
            }

            dest_lst = dest_lst.Distinct().ToList();

            var src_lst = new List<string>();
            var dmTargetS = File.ReadAllText(state.Locale + root);
            var dmTarget = DreamMap.FromString(dmTargetS);

            src_lst.AddRange(dmTarget.Tokens.SelectMany(x => x.Value.Contents.Select(y => y.ObjName)).Distinct());

            var result = src_lst.Where(x => dest_lst.IndexOf(x) == -1).ToList();
            result.Sort();

        }

        static void FCustom2(NightmareState state, IList<string> parameters)
        {
            var targ = state.Target;
            var lines = new List<string>();
            var turfs = targ.Tokens.Where(x => x.Value.Contents != null && x.Value.Contents.Where(y => y.ObjName.StartsWith("/turf/")).Count() > 1);
            var bdf = new ByondDefaultFormat();
            var textlike = String.Join("\n", turfs.Select(x => x.Key + ":" + string.Join(",",x.Value.Contents.Where(y=>y.ObjName.StartsWith("/turf/")).Select(y=>y.ObjName))));
            var textlike2 = String.Join("\n", turfs.Select(x => x.Key + ":" + x.Value.ToDreamString(bdf)));
            var usages = turfs.Select(x => x.Value.Usages).Sum();
        }

        static void FCrop(NightmareState state, IList<string> parameters)
        {
            var dmCropS = File.ReadAllText(state.Locale + parameters[0]);
            var dmCrop = DreamMap.FromString(dmCropS);
            dmCrop.Rebase(dmCrop.TokSize);
            state.Crop = dmCrop;

            var pt = new Point(state.Point.X - 1, state.Crop.Map.GetLength(0) - state.Point.Y - state.Size.Height + 1);
            if (!state.Size.IsEmpty)
                state.Crop = dmCrop.SubMap(pt, state.Size, state.GenerateCodes);
            state.Point = new Point();
            state.Size = new Size();
            if (state.Logging)
                ConsoleLog(">>Read file for crop at " + state.Locale + parameters[0]);
        }

        static void FRebase(NightmareState state, IList<string> parameters)
        {
            state.Target.Rebase(state.Target.TokSize);
            if (state.Logging)
                ConsoleLog(">>REBASED TARGET. New max Key is "+DreamMap.GenerateKey(state.Target.BiggestKey,state.Target.TokSize));
        }

        static void FTarget(NightmareState state, IList<string> parameters)
        {
            var dmTargetS = File.ReadAllText(state.Locale + parameters[0]);
            var dmTarget = DreamMap.FromString(dmTargetS);
            state.Target = dmTarget;
            if (state.Logging)
                ConsoleLog(">>Read file for target at " + state.Locale + parameters[0]);
        }

        static void FSearch(NightmareState state, IList<string> parameters)
        {
            var result = state.Target.Tokens.Where(x => x.Value.Contents.Any(y => y.ObjName.Contains(parameters[0]))).Select(x => x.Value.Code);
            state.Parameters.RemoveAt(state.Parameters.Count - 1);
            state.Parameters.Add(string.Join(';', result));
        }

        static void FSearchCrop(NightmareState state, IList<string> parameters)
        {
            var result = state.Crop.Tokens.Where(x => x.Value.Contents.Any(y => y.ObjName.Contains(parameters[0]))).Select(x => x.Value.Code);
            state.Parameters.RemoveAt(state.Parameters.Count - 1);
            state.Parameters.Add(string.Join(';', result));
        }

        static void FGenCodes(NightmareState state, IList<string> parameters)
        {
            state.GenerateCodes = true;
        }

        static void FGenCodesOff(NightmareState state, IList<string> parameters)
        {
            state.GenerateCodes = false;
        }

        static void FLogging(NightmareState state, IList<string> parameters)
        {
            state.Logging = true;
        }

        static void FLoggingOff(NightmareState state, IList<string> parameters)
        {
            state.Logging = false;
        }

        static void FUnsafe(NightmareState state, IList<string> parameters)
        {
            state.Unsafe = true;
            //ALWAYS log this
            ConsoleLog(">>UNSAFE MODE ON. CONTINUE AT YOUR OWN RISK.");
        }

        static void FSupersafe(NightmareState state, IList<string> parameters)
        {
            state.SupersafeMode = true;
        }

        static void FPoint(NightmareState state, IList<string> parameters)
        {
            var x = 0;
            var y = 0;
            if(parameters.Count==1)
            {
                var splt = parameters[0].Split(';');
                x = int.Parse(splt[0]);
                y = int.Parse(splt[1]);
            }
            else
            {
                x = int.Parse(parameters[0]);
                y = int.Parse(parameters[1]);
            }
            state.Point = new Point(x, y);
            if (state.Logging)
                ConsoleLog(">>Point is set to " + state.Point.ToString());
        }

        static void FSize(NightmareState state, IList<string> parameters)
        {
            var w = 0;
            var h = 0;
            if (parameters.Count == 1)
            {
                var splt = parameters[0].Split(';');
                w = int.Parse(splt[0]);
                h = int.Parse(splt[1]);
            }
            else
            {
                w = int.Parse(parameters[0]);
                h = int.Parse(parameters[1]);
            }
            state.Size = new Size(w, h);
            if (state.Logging)
                ConsoleLog(">>Size is set to " + state.Size.ToString());
        }

        static void FLocale(NightmareState state, IList<string> parameters)
        {
            var p = parameters[0].Trim();
            if (!string.IsNullOrEmpty(p) && !p.EndsWith('\\'))
                p += "\\";
            state.Locale = p;
            if (state.Logging)
                ConsoleLog(">>Locale is set to " + state.Locale);
        }

        static void FSave(NightmareState state, IList<string> parameters)
        {
            if (state.Target == null)
                File.WriteAllText(state.Locale + parameters[0], state.Crop.ToDreamString(state.MapFormat));
            else
                File.WriteAllText(state.Locale + parameters[0], state.Target.ToDreamString(state.MapFormat));
            if (state.Logging)
                ConsoleLog(">>Saved to " + state.Locale + parameters[0]);
        }

        static void FMake(NightmareState state, IList<string> parameters)
        {
            if (state.Target == null)
            {
                if (state.Logging)
                    ConsoleLog(">>No Target for Make");
                return;
            }
            var pt = new Point(state.Point.X - 1, state.Target.Map.GetLength(0) - state.Point.Y - state.Crop.Map.GetLength(0) + 1);
            state.Point = new Point();
            state.Target.InjectMap(state.Crop, pt, injectKey: state.Whitekey, splice: state.Merge);

            if (state.Logging)
                ConsoleLog(">>Made changes");
        }

        static void FRun(NightmareState state, IList<string> parameters)
        {
            var ret = 0;
            while (ret != -1)
            {
                var line = Console.ReadLine();
                var split = line.Split(' ');
                if (split.Length == 0)
                    return;
                ret = Run(new List<string>(split));
            }
            return;
        }

        static void FWhiteKey(NightmareState state, IList<string> parameters)
        {
            state.Whitekey = parameters[0];
            if (state.Logging)
                ConsoleLog(">>Whitekey is set to " + state.Whitekey);
        }

        static void FMerge(NightmareState state, IList<string> parameters)
        {
            state.Merge = true;
        }

        static void FMergeOff(NightmareState state, IList<string> parameters)
        {
            state.Merge = false;
        }

        static void FLog(NightmareState state, IList<string> parameters)
        {
            if(state.Logging)
                ConsoleLog(parameters[0]);
        }

        static void FEcho(NightmareState state, IList<string> parameters)
        {
            ConsoleLog(parameters[0]);
        }

        static void FNewCommand(NightmareState state, IList<string> parameters)
        {
            if (!state.Command.ContainsKey(parameters[0]))
            {
                state.Command.Add(parameters[0], parameters[2]);
                state.CommandSize.Add(parameters[0], int.Parse(parameters[1]));
            }
            if (state.Logging)
                ConsoleLog(">>Created Command " + parameters[0] + " with " + parameters[1] + " parameters");
        }

        static void FCallCommand(NightmareState state, IList<string> parameters)
        {
            var pp = parameters[0];
            if (!state.Command.ContainsKey(pp))
            {
                if (state.Logging)
                    ConsoleLog(">>Command " + pp + " is not defined");
                return;
            }
            if (state.CommandSize[pp] != parameters.Count - 1)
            {
                if (state.Logging)
                    ConsoleLog(">>Command " + pp + " has wrong number of arguments. Parameters found: " + (parameters.Count - 1) + ", Parameters Expected: " + state.CommandSize[pp]);
                return;
            }

            var cmd = state.Command[pp];

            for (var i = 1; i < parameters.Count; i++)
            {
                cmd = cmd.Replace("##" + i, parameters[i]);
            }

            var split = cmd.Split(' ');
            if (split.Length == 0)
                return;
            Run(new List<string>(split));
        }

        static void FClearTarget(NightmareState state, IList<string> parameters)
        {
            state.Target = null;
        }

        static void FClearCrop(NightmareState state, IList<string> parameters)
        {
            state.Crop = null;
            state.Whitekey = null;
        }

        static void FScriptLocale(NightmareState state, IList<string> parameters)
        {
            var p = parameters[0].Trim();
            if (!string.IsNullOrEmpty(p) && !p.EndsWith('\\'))
                p += "\\";
            state.Scriptlocale = p;
            if (state.Logging)
                ConsoleLog(">>Script locale is set to " + state.Scriptlocale);
        }

        static void FClearWhitekey(NightmareState state, IList<string> parameters)
        {
            state.Whitekey = null;
        }

        static void FTargetToCrop(NightmareState state, IList<string> parameters)
        {
            state.Crop = state.Target;
            if (state.Logging)
                ConsoleLog(">>Target stored in crop.");
        }

        static void FRemoveObject(NightmareState state, IList<string> parameters)
        {
            if (state.Target == null)
            {
                if (state.Logging)
                    ConsoleLog(">>Target does not exist to change tokens.");
                return;
            }
            var toks = state.Target.Tokens.ToList();//.Where(x => x.Value.Contents.Any(y => y.ObjName.Contains(parameters[0])));
            for (var i = 0; i < toks.Count; i++)
            {
                var t = toks[i];
                var toRemove = t.Value.Contents.Where(y => y.ToDreamString(state.MapFormat).Contains(parameters[0])).ToList();
                if (toRemove.Count == 0)
                    continue;
                for (var q = 0; q < toRemove.Count; q++)
                    state.Target.Tokens[t.Key].Contents.Remove(toRemove[q]);
            }
        }

        static void FRemoveTurf(NightmareState state, IList<string> parameters)
        {
            if (state.Target == null)
            {
                if (state.Logging)
                    ConsoleLog(">>Target does not exist to change tokens.");
                return;
            }
            if (state.Whitekey == null || !state.Target.Tokens.ContainsKey(state.Whitekey))
            {
                if (state.Logging)
                    ConsoleLog(">>Whitekey is required for deletion.");
                return;
            }

            var toks = state.Target.Tokens.Where(x => x.Value.Contents.Any(y => y.ObjName.Contains(parameters[0]))).Select(x => x.Key);

            var wkey = state.Target.Tokens[state.Whitekey];
            for (var y = 0; y < state.Target.Map.GetLength(0); y++)
                for (var x = 0; x < state.Target.Map.GetLength(1); x++)
                {
                    if (toks.Contains(state.Target.Map[y, x].Code))
                        state.Target.Map[y, x] = wkey;
                }

            foreach (var t in toks)
            {
                state.Target.Tokens.Remove(t);
            }
        }

        static void FEmptyMap(NightmareState state, IList<string> parameters)
        {
            if (state.Size.IsEmpty)
            {
                if (state.Logging)
                    ConsoleLog(">>Size is empty.");
                return;
            }

            var toks = DreamToken.FromString(parameters[0]);

            var dmap = new DreamMap();
            dmap.Map = new DreamToken[state.Size.Height, state.Size.Width];
            dmap.Tokens.Add("aaa", toks);
            dmap.Tokens["aaa"].Code = "aaa";

            for (var y = 0; y < dmap.Map.GetLength(0); y++)
                for (var x = 0; x < dmap.Map.GetLength(1); x++)
                {
                    dmap.Map[y, x] = toks;
                }

            state.Target = dmap;

            state.Size = new Size();

            if (state.Logging)
                ConsoleLog(">>New map created.");
        }

        static void FConfigRun(NightmareState state, IList<string> parameters)
        {
            ConfigRunner.ConfigRun(state.Scriptlocale + parameters[0], state);
        }

        static void FSwap(NightmareState state, IList<string> parameters)
        {
            var st = state.Crop;
            state.Crop = state.Target;
            state.Target = st;
            if (state.Logging)
                ConsoleLog(">>Swapped");
        }

        static void FSetVar(NightmareState state, IList<string> parameters)
        {
            if (!state.Variables.ContainsKey(parameters[0]))
                state.Variables.Add(parameters[0], "");
            state.Variables[parameters[0]] = parameters[1];
            if (state.Logging)
                ConsoleLog(">>Variable "+ parameters[0] + " set");
        }

        static void FGetVar(NightmareState state, IList<string> parameters)
        {
            var lastidx = parameters.Count - 1;
            var param = parameters[lastidx];

            state.Parameters = state.Parameters.SkipLast(parameters.Count).ToList();
            state.Parameters.Add(state.Variables.ContainsKey(param)? state.Variables[param] : "");
        }

        static void FUnsetVar(NightmareState state, IList<string> parameters)
        {
            if(state.Variables.ContainsKey(parameters[0]))
                state.Variables.Remove(parameters[0]);
            if (state.Logging)
                ConsoleLog(">>Variable " + parameters[0] + " unset");
        }

        static void FMapLocate(NightmareState state, IList<string> parameters)
        {
            var locateObj = parameters[0];
            var locateVar = parameters.Count > 1 ? parameters[1] : null;
            var locateValue = parameters.Count > 2 ? parameters[2] : null;
            var idx = parameters.Count > 3 ? int.Parse(parameters[3]) : 0;

            state.Parameters = state.Parameters.SkipLast(parameters.Count).ToList();

            if (state.Target == null)
            {
                if (state.Logging)
                    ConsoleLog(">>Target does not exist to change tokens.");
                return;
            }
            
            var startX = 0;
            var startY = 0;
            var sizeX = state.Target.Map.GetLength(1);
            var sizeY = state.Target.Map.GetLength(0);

            if (!state.Point.IsEmpty)
            {
                startX = state.Point.X - 1;
                startY = state.Target.Map.GetLength(0) - state.Point.Y + 1;
                state.Point = new Point();
            }

            if(!state.Size.IsEmpty)
            {
                sizeX = state.Size.Width;
                sizeY = state.Size.Height;
                state.Size = new Size();
            }

            var locatedToks = state.Target.Tokens.Where(
                x =>
                    x.Value.Contents.Any(y =>
                        {
                            if (y.ObjName != locateObj)
                                return false;
                            if (string.IsNullOrWhiteSpace(locateVar))
                                return true;
                            if (!y.Parameters.ContainsKey(locateVar))
                                return false;
                            if (string.IsNullOrWhiteSpace(locateValue))
                                return true;
                            return y.Parameters[locateVar] == locateValue;                        
                        }
                    )
                ).Select(x=>x.Value).ToList();

            var result = new List<Point>();

            for(var y=startY;y<startY+sizeY; y++)
                for(var x=startX; x<startX+sizeX;x++)
                {
                    if (locatedToks.Contains(state.Target.Map[y, x]))
                        result.Add(new Point(x+1, state.Target.Map.GetLength(0) - y));
                }

            if (result.Count > idx)
                state.Parameters.Add(result[idx].X + ";" + result[idx].Y);
            else
                state.Parameters.Add("0;0");
        }

        static void FDistance(NightmareState state, IList<string> parameters)
        {
            state.Parameters = state.Parameters.SkipLast(parameters.Count).ToList();
            if (state.Point.IsEmpty)
            {
                if (state.Logging)
                    ConsoleLog(">>Source point is empty. 0;0 added.");
                state.Parameters.Add("0;0");
                return;
            }
            var x = 0;
            var y = 0;
            if (parameters.Count == 1)
            {
                var splt = parameters[0].Split(';');
                x = int.Parse(splt[0]);
                y = int.Parse(splt[1]);
            }
            else
            {
                x = int.Parse(parameters[0]);
                y = int.Parse(parameters[1]);
            }
            var pt = state.Point;
            state.Parameters.Add(
                (x > pt.X ? x - pt.X + 1 : pt.X - x + 1).ToString()
                + ";" +
                (y > pt.Y ? y - pt.Y + 1 : pt.Y - y + 1).ToString());
        }

        static void FConcat(NightmareState state, IList<string> parameters)
        {
            state.Parameters = state.Parameters.SkipLast(parameters.Count).ToList();
            state.Parameters.Add(string.Join("", parameters));
        }

        static void FClearVars(NightmareState state, IList<string> parameters)
        {
            var keys = state.Variables.Keys.ToList();
            foreach (var key in keys)
            {
                state.Variables.Remove(key);
                if (state.Logging)
                    ConsoleLog(">>Variable " + key + " unset");
            }
        }

        static void FTransform(NightmareState state, IList<string> parameters)
        {
            if (state.Target == null)
            {
                if (state.Logging)
                    ConsoleLog(">>Target is empty.");
                return;
            }

            var transform = parameters[0].ToLower();

            var dmap = new DreamMap();
            if(transform == "mirrord1" || transform == "mirrord2" || transform == "rotate90" || transform == "rotate270")
                dmap.Map = new DreamToken[state.Target.Map.GetLength(1),state.Target.Map.GetLength(0)];
            else
                dmap.Map = new DreamToken[state.Target.Map.GetLength(0), state.Target.Map.GetLength(1)];
            dmap.Tokens = state.Target.Tokens;
            dmap.XOffset = state.Target.XOffset;
            dmap.YOffset = state.Target.YOffset;
            dmap.ZLevel = state.Target.ZLevel;
            dmap.TokSize = state.Target.TokSize;

            if(transform == "mirrorv")
                for (var y = 0; y < dmap.Map.GetLength(0); y++)
                    for (var x = 0; x < dmap.Map.GetLength(1); x++)
                    {
                        dmap.Map[y, x] = state.Target.Map[y, dmap.Map.GetLength(1)-x-1];
                    }

            if (transform == "mirrorh")
                for (var y = 0; y < dmap.Map.GetLength(0); y++)
                    for (var x = 0; x < dmap.Map.GetLength(1); x++)
                    {
                        dmap.Map[y, x] = state.Target.Map[dmap.Map.GetLength(0) - y - 1,  x];
                    }

            if (transform == "mirrord1")
                for (var y = 0; y < dmap.Map.GetLength(0); y++)
                    for (var x = 0; x < dmap.Map.GetLength(1); x++)
                    {
                        dmap.Map[y, x] = state.Target.Map[x,y];
                    }

            if (transform == "rotate270")
                for (var y = 0; y < dmap.Map.GetLength(0); y++)
                    for (var x = 0; x < dmap.Map.GetLength(1); x++)
                    {
                        dmap.Map[y, dmap.Map.GetLength(1) - x - 1] = state.Target.Map[x,  y];
                    }

            if (transform == "rotate90")
                for (var y = 0; y < dmap.Map.GetLength(0); y++)
                    for (var x = 0; x < dmap.Map.GetLength(1); x++)
                    {
                        dmap.Map[dmap.Map.GetLength(0) - y - 1, x] = state.Target.Map[x, y];
                    }

            if (transform == "mirrord2")
                for (var y = 0; y < dmap.Map.GetLength(0); y++)
                    for (var x = 0; x < dmap.Map.GetLength(1); x++)
                    {
                        dmap.Map[y, x] = state.Target.Map[dmap.Map.GetLength(1)-x-1, dmap.Map.GetLength(0)-y-1];
                    }

            state.Target = dmap;
            if (state.Logging)
                ConsoleLog(">>New map created.");
        }

        static void FSetFormat(NightmareState state, IList<string> parameters)
        {
            var key = parameters[0];
            if(!Program.MapFormats.ContainsKey(key))
            {
                if (state.Logging)
                    ConsoleLog(">>Save Format " + key + " failed. No such Key");
                return;
            }

            state.MapFormat = Program.MapFormats[key];


            if (state.Logging)
                ConsoleLog(">>Save Format " + key + " is now set");
            
        }

        static void FObjectDefault(NightmareState state, IList<string> parameters)
        {
            if (state.Target == null)
            {
                if (state.Logging)
                    ConsoleLog(">>Target is empty.");
                return;
            }

            var keyname = parameters[0];
            var fieldname = parameters[1];
            var value = parameters[2];

            foreach (var tok in state.Target.Tokens)
                foreach (var subtok in tok.Value.Contents)
                    if (subtok.ObjName.IndexOf(keyname) != -1)
                    {
                        if (subtok.Parameters == null)
                            subtok.Parameters = new Dictionary<string, string>();
                        if (!subtok.Parameters.Any(x => x.Key == fieldname))
                            subtok.Parameters.Add(fieldname, value);
                    }
        }

        static void FRotateDir(NightmareState state, IList<string> parameters)
        {
            if (state.Target == null)
            {
                if (state.Logging)
                    ConsoleLog(">>Target is empty.");
                return;
            }

            var dirname = parameters[0];
            var angle = int.Parse(parameters[1]);

            foreach (var tok in state.Target.Tokens)
                foreach (var subtok in tok.Value.Contents)
                    if (subtok.Parameters != null)
                    {
                        var lst = subtok.Parameters.Keys.ToList();
                        for (var i = 0; i < lst.Count; i++)
                        {
                            var par = lst[i];
                            if (par == dirname)
                                subtok.Parameters[par] = ((int)((Direction)int.Parse(subtok.Parameters[par])).Rotate(angle)).ToString();
                        }
                    }
        }

        static void FFlipDir(NightmareState state, IList<string> parameters)
        {
            if (state.Target == null)
            {
                if (state.Logging)
                    ConsoleLog(">>Target is empty.");
                return;
            }

            var dirname = parameters[0];
            var angle = int.Parse(parameters[1]);

            foreach (var tok in state.Target.Tokens)
                foreach (var subtok in tok.Value.Contents)
                    if (subtok.Parameters != null)
                    {
                        var lst = subtok.Parameters.Keys.ToList();
                        for (var i = 0; i < lst.Count; i++)
                        {
                            var par = lst[i];
                            if (par == dirname)
                                subtok.Parameters[par] = ((int)((Direction)int.Parse(subtok.Parameters[par])).Rotate(angle)).ToString();
                        }
                    }
        }

        #endregion
    }

    public enum Direction
    {
        North = 1,
        South = 2,
        East = 4,
        West = 8,
        NorthEast = 5,
        NorthWest = 9,
        SouthEast = 6,
        SouthWest = 10
    }

    public static class DirectionExtensionMethods
    {
        private static List<Direction> order = new List<Direction>()
        {
            Direction.South, Direction.SouthEast, Direction.East, Direction.NorthEast, Direction.North, Direction.NorthWest, Direction.West, Direction.SouthWest
        };
        public static Direction Rotate(this Direction dir, int angle)
        {
            if (angle == 0)
                return dir;
            var shift = (order.IndexOf(dir) + angle) % order.Count;
            return order[shift];
        }
        public static Direction Flip(this Direction dir)
        {
            return dir.Rotate(order.Count / 2);
        }
    }

    public static class StringExtensionMethods
    {
        public static string ReplaceFirst(this string text, string search, string replace)
        {
            int pos = text.IndexOf(search);
            if (pos < 0)
            {
                return text;
            }
            return text.Substring(0, pos) + replace + text.Substring(pos + search.Length);
        }
    }

    class ChanceItem
    {
        public int Chance { get; set; }
        public List<string> Commands { get; } = new List<string>();
        public List<ChanceBlock> Blocks { get; } = new List<ChanceBlock>();
        public ChanceBlock Parent { get; set; }
    }

    class ChanceBlock
    {
        public string BlockName { get; set; }
        public List<ChanceItem> Items { get; } = new List<ChanceItem>();
        public List<string> ConditionVars { get; set; }
        public int TotalChance
        {
            get
            {
                return Items.Sum(x => x.Chance);
            }
        }
        public ChanceItem Parent { get; set; }
    }

    static class ConfigRunner
    {

        public static void ConfigRun(string configFile, NightmareState state)
        {
            var root = new ChanceItem()
            {
                Chance = 100
            };
            ChanceItem item = root;
            ChanceBlock block = null;
            var lines = File.ReadAllLines(configFile);
            var currentTab = 0;
            var blocksmode = false;
            for (var i = 0; i < lines.Length; i++)
            {
                if (lines[i].Trim().StartsWith("//"))
                    continue;
                var tab = lines[i].TakeWhile(x => x == '\t').Count();
                var line = lines[i].Trim();
                if (line.StartsWith('@') && tab == currentTab)
                {
                    if (blocksmode)
                        throw new Exception("Blocks cannot be created under another block without chance descriptor.");
                    blocksmode = true;
                    var splts = line.Split('|');
                    block = new ChanceBlock()
                    {
                        Parent = item,
                        BlockName = splts[0].TrimStart('@'),
                        ConditionVars = splts.Where(x=>x.StartsWith('#')).Select(x=>x.TrimStart('#')).ToList()
                    };
                    item.Blocks.Add(block);
                    item = null;
                    continue;
                }

                if (blocksmode)
                {
                    item = new ChanceItem()
                    {
                        Parent = block,
                        Chance = int.Parse(line)
                    };
                    block.Items.Add(item);
                    block = null;
                    blocksmode = false;
                    currentTab = tab + 1;
                    continue;
                }

                if (currentTab == tab)
                {
                    if (item == null)
                        throw new Exception("Wrong block order");
                    item.Commands.Add(lines[i].Trim());
                    continue;
                }

                if (tab > currentTab)
                    throw new Exception("Too many tabulations spotted");

                while (tab < currentTab)
                {
                    if (item == null)
                    {
                        blocksmode = false;
                        item = block.Parent;
                        block = null;
                        currentTab--;
                    }
                    else
                    {
                        blocksmode = true;
                        block = item.Parent;
                        item = null;
                        currentTab--;
                    }
                }
                i--;//try again brother
            }

            //execution time
            RunItem(root, state);
        }

        static void RunItem(ChanceItem item, NightmareState state)
        {
            var rnd = new Random();
            for (var i = 0; i < item.Commands.Count; i++)
                DefaultCommands.Run(new List<string>(item.Commands[i].Split(' ')));

            for (var i = 0; i < item.Blocks.Count; i++)
            {
                var blk = item.Blocks[i];
                var blockRnd = rnd.Next(blk.TotalChance) + 1;
                var stop = false;
                for (var p = 0; p < blk.ConditionVars.Count; p++)
                {
                    var splt = blk.ConditionVars[p].Split("=");
                    var vl = splt[0];
                    var negate = false;
                    if (vl.StartsWith('!'))
                    {
                        vl = vl.TrimStart('!');
                        negate = true;
                    }
                    if (splt.Length == 1 && (!negate == (!state.Variables.ContainsKey(vl) || string.IsNullOrEmpty(state.Variables[vl]))))
                        stop = true;
                    if (splt.Length == 2 && (!negate == (!state.Variables.ContainsKey(vl) || state.Variables[vl].Trim()!=splt[1].Trim())))
                        stop = true;
                }
                if (stop)
                    continue;
                for (var k = 0; k < blk.Items.Count; k++)
                {
                    var thing = blk.Items[k];
                    if (blockRnd < thing.Chance)
                    {
                        RunItem(thing, state);
                        break;
                    }
                    blockRnd -= thing.Chance;
                }
            }
        }
    }
}
