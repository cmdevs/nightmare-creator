import asyncio,bz2,re,os
from subprocess import run,PIPE,Popen
import struct,socket
import shutil
import json

reboot = False
resets = 0
scriptpath = ""
byondpath = ""

def byond_topic(data):
	data = bytes(data,'latin-1')
	sdata = ('localhost',1400)
	pref = b'\x00\x83\x00'
	leng = struct.pack('b',len(data)+6)
	pad = b'\x00'*5
	suff = b'\x00'

	b_msg = pref+leng+pad+data+suff
	s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
	s.settimeout(2)
	s.connect(sdata)
	s.send(b_msg)
	ret = s.recv(10240)[5:-1]
	s.close()
	return ret.decode('latin-1')

def search(ckey):
	ckey = ckey.lower()
	re = ''
	for di,f,d in os.walk(scriptpath+"ColonialMarinesALPHA\\data\\player_saves\\"):
		if ckey in di and "@" not in di:
			found = di.rpartition("\\")[-1]
			re += '[[[a href=?notes='+found+' target=_blank]]]'+found+'[[[/a]]]' + "\n"
	return re

def call_pipe(arg):
	return run(arg,stdout=PIPE,stderr=PIPE,universal_newlines=True)

def ls_file(directory):
	for di,d,f in os.walk(directory):
		return f

def ls_dir(directory):
	for di,d,f in os.walk(directory):
		return d

def update(param):
	print("Performing remote update.\n")
	r = "Performing remote update.\n"
	maps = ls_file(scriptpath+"ColonialMarinesALPHA\\maps\\")
	for map in maps:
		if "_tocompile" in map:
			continue
		if map[-4:].lower() == ".dmm" and map[0] == 'Z':
			shutil.copy(scriptpath+"ColonialMarinesALPHA\\maps\\"+map+".bak",scriptpath+"ColonialMarinesALPHA\\maps\\"+map)
	r += "Restoring modifed LV624.dmm from original\n"
	x = call_pipe(scriptpath+'updatefull.bat')
	r += x.stdout
	r += "Updating Nightmare Creator\n"
	x = call_pipe(scriptpath+'update_nightmare.bat')
	r += x.stdout
	r += "Overwriting Nightmare Creator configuration file\n"
	for map in maps:
		if "_tocompile" in map:
			continue
		if map[-4:].lower() == ".dmm" and map[0] == 'Z':
			r += "\nUpdating "+map+" backup file"
			shutil.copy(scriptpath+"ColonialMarinesALPHA\\maps\\"+map,scriptpath+"ColonialMarinesALPHA\\maps\\"+map+".bak")
		r += "Running the Nightmare Creator"
		r += nightmare(False)
	return r

def freset(param):
	print("Forcing a hard reset.\n")
	r = 'Performing hard reset.\n'
	x = call_pipe("taskkill /f /im dreamdaemon.exe")
	r += x.stdout+"\n"
	x = call_pipe("taskkill /f /im java.exe")
	Popen(byondpath+"dreamdaemon.exe "+scriptpath+"CMALPHA\\ColonialMarinesALPHA.dmb 1400 -trusted",shell=True)
	return r+x.stdout

def sreset(param):
	print("Performing a scheduled reset.\n")
	r = 'Performing a scheduled reset.\n'
	x = call_pipe("taskkill /f /im dreamdaemon.exe")
	r += x.stdout+"\n"
	x = call_pipe("taskkill /f /im java.exe")
	Popen(byondpath+"dreamdaemon.exe "+scriptpath+"CMALPHA\\ColonialMarinesALPHA.dmb 1400 -trusted",shell=True)
	return r+x.stdout
	
def nightmare(params):
	print("Running the Nightmare Creator")
	maps = ls_file(scriptpath+"ColonialMarinesALPHA\\maps\\")
	for map in maps:
		if map[-4:].lower() == ".dmm" and map[0] == 'Z' and not "tocompile" in map:
			shutil.copy(scriptpath+"ColonialMarinesALPHA\\maps\\"+map+".bak",scriptpath+"ColonialMarinesALPHA\\maps\\"+map)
	print("LV Backed up")
	r = "Resetting maps to original files\nRunning the Nightmare Creator\n"
	print("Running Creator")
	x = call_pipe("NightmareCreator.exe -loc ColonialMarinesALPHA/maps/ -loc-scripts ColonialMarinesALPHA/maps/Nightmare/config -run shared.cfg")
	r += x.stdout
	try:
		for map in maps:
			try:
				os.stat(scriptpath+"ColonialMarinesALPHA\\maps\\"+map.replace(".dmm","_tocompile.dmm"))
			except:
				continue
			if ".bak" in map:
				continue
			if map[-4:].lower() == ".dmm" and map[0] == 'Z':
				r += "Copying "+map.replace(".dmm","_tocompile.dmm")+" over "+map+"\n"
				print("Copying "+map.replace(".dmm","_tocompile.dmm")+" over "+map+"\n")
				shutil.copy(scriptpath+"ColonialMarinesALPHA\\maps\\"+map.replace(".dmm","_tocompile.dmm"), scriptpath+"ColonialMarinesALPHA\\maps\\"+map)
	except shutil.Error as e:
		print("Error: "+e)
	except IOError as e:
		print("IOError: "+e)
	r += "\nProcess done"
	return r

def kill(param):
	print("Killing server process.\n")
	r = 'Killing server process.\n'
	x = call_pipe("taskkill /f /im dreamdaemon.exe")
	return r+x.stdout

def updatelogs(param):
	call_pipe(scriptpath+"winscp.bat")
	return "Remote logs updated."

def get_notes(ckey):
	return byond_topic("?notes&ckey="+ckey).replace('<br/>','\n')

def sched_reboot(param):
	global reboot
	reboot = not reboot
	if reboot:
		print("A hard reboot has been scheduled for the next round restart.")
		return "A hard reboot has been scheduled for the next round restart."
	else:
		print("The scheduled hard reboot has been canceled.")
		return "The scheduled hard reboot has been canceled."

def reset_hard(param):
	x = call_pipe(scriptpath+"gpanel\\gitreset.bat")
	r = x.stdout + "\nGit hard reset performed."
	print("Git hard reset performed")
	return r

def git_log(param):
	x = call_pipe(scriptpath+"gpanel\\gitlog.bat")
	tmp = x.stdout.split("\n")
	r = ''
	for i in range(0,75):
		r += tmp[i]+"\n"
	r += "End of Git log."
	print("Git log sent out.")
	return r

def get_branches(param):
	x = call_pipe(scriptpath+"gpanel\\gitbranch.bat")
	tmp = x.stdout.split("\n")
	ret = {}
	for i in tmp:
		#i = i.replace("(","").replace(")","").replace("/","").replace("  ","").replace("remotes/origin/","")
		if len(i) < 2:
			continue
		if '*' in i:
			ret[i.replace('*','').strip().replace("remotes/origin/","")] = 1
		else:
			ret[i.strip().replace("remotes/origin/","")] = 0
	val = json.dumps(ret)
	return val

def swapbr(param):
	branch = get_branches(True)
	x = 0
	ret = ''
	if "GITRESETHARDME" in param:
		param = param.replace("GITRESETHARDME","")
		x = call_pipe("git -C ColonialMarinesALPHA reset --hard")
		ret += x.stdout
	if param in branch:
		x = call_pipe("git -C ColonialMarinesALPHA checkout "+param)
		ret += x.stdout
	else:
		return ret+"\nBranch "+param+" does not exist."
	return ret

def git_branch(param):
	x = call_pipe(scriptpath+"gpanel\\gitbranch.bat")
	tmp = x.stdout.split("\n")
	r = ''
	for i in tmp:
		if '*' in i:
			r += "ACTIVE BRANCH -->"+i.replace("*","")+"<-- ACTIVE BRANCH\n"
			continue
		r += i+"\n"
	r += "End of Git log."
	print("Git log sent out.")
	return r

ops = {"update":update,"freset":freset,"killserver":kill,"updatelogs":updatelogs,"notes":get_notes,"search":search,'sreboot':sched_reboot,'gitreset':reset_hard,'gitlog':git_log,'gitbranch':git_branch,'nightmare':nightmare,'swapbr':swapbr,'qbranch':get_branches}

async def hc(r,w):
	global reboot
	global resets
	ip = w.get_extra_info("peername")[0]
	cmd = 0
	if ip != "72.5.53.3" and ip != "127.0.0.1":
		await w.drain()
		w.close()
		print("Denied access to "+ip)
		return
	print("Got connection from {}".format(ip))
	res = (await r.read(1024)).decode('latin-1').replace("\r","").replace("\n","")
	print(bytes(res,'utf-8'))
	if "rebooting=1" in res:
		resets = resets + 1
		if resets == 3:
			reboot = True
		if reboot == True:
			resets = 0
			sreset(0)
			reboot = False
		nightmare(True)
		w.write(b'HTTP 200 OK\n\n')
		await w.drain()
		w.close()
		return
	if("&&" in res):
		cmd = res.split("&&")
		res = cmd[0]
		cmd = cmd[1]
	print("Command: \""+res+"\"\n")
	task = loop.run_in_executor(None,ops[res],cmd)
	result = await task
	try:
		w.write(bytes(result.replace(scriptpath,"."),'utf-8'))
		print("Command finished.\n")
	except:
		print("Invalid command\n"+str(ops))
		w.write(b"Invalid command.\n")
		await w.drain()
		w.close()
		return
	#print("Remote update command accepted.")
	#w.write(b"Performing remote update.\n")
	await w.drain()
	#w.write(bytes(r.stdout.replace(scriptpath,"."),'utf-8'))
	w.close()
	return

loop = asyncio.get_event_loop()

coro = asyncio.start_server(hc,"0.0.0.0",8888)
srv = loop.run_until_complete(coro)
loop.create_task(coro)
try:
	loop.run_forever()
except KeyboardInterrupt:
	loop.close()
